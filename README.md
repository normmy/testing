# Prueba de ingreso

Saludos ! bienvenido, espero que estes preparado para poder iniciar con tu prueba

## Lenguaje de programación

Puedes utilizar el lenguaje de programación de tu preferencia.


## Tiempo 

la prueba tiene un tiempo de 30 minutos para los siguientes ejercicios:

### Ejercicio 1.  Cifrado de texto
Tiene un texto dado, (ejemplo: "trabajo en SIN"). 

Reemplace espacios por simbolo $ y acomode las letras en orden de arriba hacia abajo de izquierda a derecha en una matriz NxN donde los espacios extras que no utilice tambien vayan con $ como se da el ejemplo.
````
    [ t a e I ]
    [ r j n N ]
    [ a o $ $ ]
    [ b $ S $ ]
````
Concatene de derecha a Izquierda por fila y obtendra:
````
 taeIrjnNao$$b$S$ 
 ````

Genere un método para desencriptar textos generados de esta manera y encuentre el mensaje en 
Texto: 
````
Faloccaedo,iodle$$cmoisleip.c!ojol$i,ge1e$d$rr$t$
````


### Ejercicio 2. API REST
Mediante un script del lenguaje que desee:
1. Consuma el siguiente endpoint GET:

https://reqres.in/api/users

2. Formatee el resultado de los elementos en formato HTML y guarde de manera automática en un archivo para su revisión. 



## Método de entrega. 

Genere un nuevo branch con su nombre en este repositorio y publique los archivos con el código fuente que utilizó para resolver los ejercicios, 
genere un archivo README.md con instrucciones sobre como ejecutar los archivos. 


En caso de no poder realizarlo, envíe los archivos en un documento compreso a normmy@gmail.com



**Te deseo éxito porque suerte se le desea a los mediocres**

